/* 75x113 keycap area
 * 
 */

$fn = 64;
pcb_width = 81;
pcb_length = 119;

 module kailh () {
    kailh_height=2;
    kailh_corner_radius=2;
    kailh_width=10.8;
    kailh_length=6;
    
    translate([0,0,-kailh_height]) color("grey") difference() {
        hull() {
            cube([kailh_corner_radius,kailh_corner_radius,kailh_height]);
            translate([kailh_corner_radius,4,0]) cylinder(h=kailh_height,r=kailh_corner_radius);
            translate([kailh_width-kailh_corner_radius,0,0]) cube([kailh_corner_radius,kailh_corner_radius,kailh_height]);
            translate([kailh_width-kailh_corner_radius,kailh_length-kailh_corner_radius,0]) cube([kailh_corner_radius,kailh_corner_radius,kailh_height]);
        }
        translate([3.7+kailh_corner_radius,0,0]) cylinder(h=kailh_height, r=kailh_corner_radius);
        translate([3.7+kailh_corner_radius,0,0]) cube([kailh_width-3.7-kailh_corner_radius,kailh_corner_radius,kailh_height]);
    }
}

module switch () {
    switch_width = 14;
    switch_length = 14;
    switch_height = 11.5;

    cube([switch_width,switch_length,switch_height]);
    translate([switch_width/2-2,switch_length/2-0.625,switch_height]) cube([4,1.25,3.5]);
    translate([switch_width/2-0.625,switch_length/2-2,switch_height]) cube([1.25,4,3.5]);
}

module pcb() {
    pcb_thickness = 1.6;
    pcb_corner_radius = 5;
    
    translate([pcb_corner_radius,pcb_corner_radius,0]) hull() {
        cylinder(h=pcb_thickness, r=pcb_corner_radius);
        translate([pcb_width-2*pcb_corner_radius,0,0]) cylinder(h=pcb_thickness, r=pcb_corner_radius);
        translate([0,pcb_length-2*pcb_corner_radius,0]) cylinder(h=pcb_thickness, r=pcb_corner_radius);
        translate([pcb_width-2*pcb_corner_radius,pcb_length-2*pcb_corner_radius,0]) cylinder(h=pcb_thickness, r=pcb_corner_radius);
    }
}

translate([15.5,12.8,0]) kailh();   // 0
translate([14.5,5,0]) switch();
translate([44,12.8,0]) kailh();     // .
translate([43,5,0]) switch();
translate([63,22.3,0]) kailh();     // enter
translate([62,14.5,0]) switch();
translate([44,31.8,0]) kailh();     // 3
translate([43,24,0]) switch();
translate([25,31.8,0]) kailh();     // 2
translate([24,24,0]) switch();
translate([6,31.8,0]) kailh();      // 1
translate([5,24,0]) switch();
translate([44,50.8,0]) kailh();     // 6
translate([43,43,0]) switch();
translate([25,50.8,0]) kailh();     // 5
translate([24,43,0]) switch();
translate([6,50.8,0]) kailh();      // 4
translate([5,43,0]) switch();
translate([63,60.3,0]) kailh();     // +
translate([63,52.5,0]) switch();
translate([44,69.8,0]) kailh();     // 9
translate([43,62,0]) switch();
translate([25,69.8,0]) kailh();     // 8
translate([24,62,0]) switch();
translate([6,69.8,0]) kailh();      // 7
translate([5,62,0]) switch();
translate([63,88.8,0]) kailh();     // -
translate([63,81,0]) switch();
translate([44,88.8,0]) kailh();     // *
translate([43,81,0]) switch();
translate([25,88.8,0]) kailh();     // /
translate([24,81,0]) switch();
translate([6,88.8,0]) kailh();      // Num
translate([5,81,0]) switch();
translate([63,107.8,0]) kailh();    // Next
translate([63,100,0]) switch();
translate([44,107.8,0]) kailh();    // Stop
translate([43,100,0]) switch();
translate([25,107.8,0]) kailh();    // Play/Pause
translate([24,100,0]) switch();
translate([6,107.8,0]) kailh();     // Prev
translate([5,100,0]) switch();
pcb();
