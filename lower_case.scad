$fn = 64;
pcb_width = 81;
pcb_length = 119;
case_thickness = 3;
case_height = 15;
pcb_thickness = 1.6;
breakout_width = 21;
breakout_length = 13;
inner_depth = 5;

module pcb(h) {
    pcb_thickness = h ? h : 1.6;
    pcb_corner_radius = 5;
    
    translate([pcb_corner_radius,pcb_corner_radius,0]) hull() {
        cylinder(h=pcb_thickness, r=pcb_corner_radius);
        translate([pcb_width-2*pcb_corner_radius,0,0]) cylinder(h=pcb_thickness, r=pcb_corner_radius);
        translate([0,pcb_length-2*pcb_corner_radius,0]) cylinder(h=pcb_thickness, r=pcb_corner_radius);
        translate([pcb_width-2*pcb_corner_radius,pcb_length-2*pcb_corner_radius,0]) cylinder(h=pcb_thickness, r=pcb_corner_radius);
    }
}

module lower_case() {
    translate([0,0,-1*case_height]) {
        difference() {
            cube([pcb_width + 2*case_thickness, pcb_length + 2*case_thickness, case_height]);
            translate([0,-1,-1*case_height/2+3.5]) rotate([-2.5,0,0]) cube([pcb_width+2*case_thickness, pcb_length+2*case_thickness+2, 10]);
            
            translate([0,(pcb_length+2*case_thickness)/2,case_height-14]) {
                rotate([0,-10,0]) cube([10,pcb_length+2*case_thickness,case_height], center=true);
                translate([pcb_width+2*case_thickness,0,0]) rotate([0,10,0]) cube([10,pcb_length+2*case_thickness,case_height], center=true);
                translate([-1.3,0,0.8]) cube([10,pcb_length+2*case_thickness,case_height], center=true);
                translate([1.3,0,0.8]) translate([pcb_width+2*case_thickness,0,0]) cube([10,pcb_length+2*case_thickness,case_height], center=true);
            }
            rotate([6.2,0,0]) cube([pcb_width+2*case_thickness,1,10]);
        }
    }
}

module standoff(x, y) {
    translate([case_thickness+x,case_thickness+y,-1*inner_depth]) difference() {
        translate([0,0,(inner_depth-pcb_thickness)/2]) cylinder(h=inner_depth-pcb_thickness, d=3.5, center=true);
        translate([0,0,(inner_depth-pcb_thickness)/2]) cylinder(h=inner_depth-pcb_thickness, d=1, center=true);
  }
}

module usb_breakout() {
    breakout_thickness = 1;

    //PCB
    cube([breakout_width, breakout_length, breakout_thickness]);

    // space for solder blobs
    translate([0,0,-1]) cube([breakout_width, 4, breakout_thickness]);

    // USB-C
    translate([breakout_width/2, breakout_length-8/2+1, -4/2]) cube([10, 8, 4], center=true);
    translate([breakout_width/2, breakout_length-8/2+1+7, -4/2]) cube([16, 8, 8], center=true);
    
    // screw holes
    translate([2.5,breakout_length-2.5,-2]) cylinder(h=2,d=1);
    translate([breakout_width-2.5,breakout_length-2.5,-2]) cylinder(h=2,d=1);
}

difference() {
    lower_case();
    translate([case_thickness, case_thickness, -1*inner_depth]) pcb(inner_depth);
    translate([(pcb_width + 2*case_thickness)/2-breakout_width/2,pcb_length + 2*case_thickness-breakout_length-case_thickness,-1*(inner_depth+1)]) usb_breakout();
}

standoff(21.5, pcb_length-97);
standoff(56, pcb_length-97.5);
standoff(21.5, pcb_length-32);
standoff(59.5, pcb_length-32);

// TODO: screw holes for top half